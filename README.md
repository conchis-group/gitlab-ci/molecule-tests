# molecule-tests
GitLab CI for ansible testing 


## Example

```
include:
  - project: "conchis-group/gitlab-ci/molecule-tests"
    ref: "main"
    file:
      - "stages.yml"
      - "linting.yml"
      - "tests.yml"
```
